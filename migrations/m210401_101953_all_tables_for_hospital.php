<?php

use yii\db\Migration;

/**
 * Class m210401_101953_all_tables_for_hospital
 */
class m210401_101953_all_tables_for_hospital extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('filial', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'description' => $this->text(),
            'phone_number1' => $this->string(30),
            'phone_number2' => $this->string(30),
            'address' => $this->text(),
            'img' => $this->string(30),
            'head_doctor_id' => $this->integer(),
            'status' => $this->smallInteger(),
            'longitude' => $this->string(40),
            'latitude' => $this->string(40),
        ]);

        $this->createTable('doctor', [
            'id' => $this->primaryKey(),
            'full_name' => $this->text(),
            'phone_number' => $this->text(),
            'work_experience' => $this->text(),
            'education' => $this->text(),
            'direction_id' => $this->integer(),
            'stage' => $this->text(),
            'img' => $this->string(40),
            'status' => $this->integer(),
            'lunch_time' => $this->string()
        ]);

        $this->createTable('direction', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'description' => $this->text(),
            'img' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('patient', [
            'id' => $this->primaryKey(),
            'full_name' => $this->text(),
            'phone_number' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('reception', [
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(),
            'filial_id' => $this->integer(),
            'doctor_id' => $this->integer(),
            'device_id' => $this->integer(),
            'date' => $this->timestamp(),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('device', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'description' => $this->text(),
            'where_produced' => $this->text(),
            'when_produced' => $this->timestamp(),
            'status' => $this->smallInteger(),
            'lunch_time' => $this->string()
        ]);

        $this->createTable('work_time_device', [
            'id' => $this->primaryKey(),
            'device_id' => $this->integer(),
            'mon' => $this->string(),
            'tues' => $this->string(),
            'wed' => $this->string(),
            'thur' => $this->string(),
            'frid' => $this->string(),
            'sat' => $this->string(),
            'sun' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('work_time_doctor', [
            'id' => $this->primaryKey(),
            'doctor_id' => $this->integer(),
            'mon' => $this->string(),
            'tues' => $this->string(),
            'wed' => $this->string(),
            'thur' => $this->string(),
            'frid' => $this->string(),
            'sat' => $this->string(),
            'sun' => $this->string(),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'sending_date' => $this->timestamp(),
            'status' => $this->smallInteger()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('filial');
        $this->dropTable('doctor');
        $this->dropTable('patient');
        $this->dropTable('message');
        $this->dropTable('direction');
        $this->dropTable('work_time_doctor');
        $this->dropTable('work_time_device');
        $this->dropTable('device');
        $this->dropTable('reception');
    }


}
