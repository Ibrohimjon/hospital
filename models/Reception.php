<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reception".
 *
 * @property int $id
 * @property int|null $patient_id
 * @property int|null $filial_id
 * @property int|null $doctor_id
 * @property int|null $device_id
 * @property string|null $date
 * @property int|null $status
 */
class Reception extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reception';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patient_id', 'filial_id', 'doctor_id', 'device_id', 'status'], 'default', 'value' => null],
            [['patient_id', 'filial_id', 'doctor_id', 'device_id', 'status'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'filial_id' => 'Filial ID',
            'doctor_id' => 'Doctor ID',
            'device_id' => 'Device ID',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
}
