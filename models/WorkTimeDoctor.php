<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_time_doctor".
 *
 * @property int $id
 * @property int|null $doctor_id
 * @property string|null $mon
 * @property string|null $tues
 * @property string|null $wed
 * @property string|null $thur
 * @property string|null $frid
 * @property string|null $sat
 * @property string|null $sun
 * @property int|null $status
 */
class WorkTimeDoctor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_time_doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'status'], 'default', 'value' => null],
            [['doctor_id', 'status'], 'integer'],
            [['mon', 'tues', 'wed', 'thur', 'frid', 'sat', 'sun'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Doctor ID',
            'mon' => 'Mon',
            'tues' => 'Tues',
            'wed' => 'Wed',
            'thur' => 'Thur',
            'frid' => 'Frid',
            'sat' => 'Sat',
            'sun' => 'Sun',
            'status' => 'Status',
        ];
    }
}
