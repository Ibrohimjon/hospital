<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filial".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $phone_number1
 * @property string|null $phone_number2
 * @property string|null $address
 * @property string|null $img
 * @property int|null $head_doctor_id
 * @property int|null $status
 * @property string|null $longitude
 * @property string|null $latitude
 */
class Filial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'address'], 'string'],
            [['head_doctor_id', 'status'], 'default', 'value' => null],
            [['head_doctor_id', 'status'], 'integer'],
            [['phone_number1', 'phone_number2', 'img'], 'string', 'max' => 30],
            [['longitude', 'latitude'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'phone_number1' => 'Phone Number1',
            'phone_number2' => 'Phone Number2',
            'address' => 'Address',
            'img' => 'Img',
            'head_doctor_id' => 'Head Doctor ID',
            'status' => 'Status',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
        ];
    }
}
