<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $phone_number
 * @property string|null $work_experience
 * @property string|null $education
 * @property int|null $direction_id
 * @property string|null $stage
 * @property string|null $img
 * @property int|null $status
 * @property string|null $lunch_time
 */
class Doctor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone_number', 'work_experience', 'education', 'stage'], 'string'],
            [['direction_id', 'status'], 'default', 'value' => null],
            [['direction_id', 'status'], 'integer'],
            [['img'], 'string', 'max' => 40],
            [['lunch_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'phone_number' => 'Phone Number',
            'work_experience' => 'Work Experience',
            'education' => 'Education',
            'direction_id' => 'Direction ID',
            'stage' => 'Stage',
            'img' => 'Img',
            'status' => 'Status',
            'lunch_time' => 'Lunch Time',
        ];
    }
}
