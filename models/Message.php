<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int|null $patient_id
 * @property int|null $user_id
 * @property string|null $message
 * @property string|null $sending_date
 * @property int|null $status
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patient_id', 'user_id', 'status'], 'default', 'value' => null],
            [['patient_id', 'user_id', 'status'], 'integer'],
            [['message'], 'string'],
            [['sending_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'user_id' => 'User ID',
            'message' => 'Message',
            'sending_date' => 'Sending Date',
            'status' => 'Status',
        ];
    }
}
