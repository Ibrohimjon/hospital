<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $where_produced
 * @property string|null $when_produced
 * @property int|null $status
 * @property string|null $lunch_time
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'where_produced'], 'string'],
            [['when_produced'], 'safe'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['lunch_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'where_produced' => 'Where Produced',
            'when_produced' => 'When Produced',
            'status' => 'Status',
            'lunch_time' => 'Lunch Time',
        ];
    }
}
