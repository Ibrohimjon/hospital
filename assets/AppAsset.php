<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "../assets/images/apple-touch-icon.png",
        "../assets/images/favicon.ico",


        "../../global/css/bootstrap.min.css",
        "../../global/css/bootstrap-extend.min.css",
        "../assets/css/site.min.css",


        "../../global/vendor/animsition/animsition.css",
        "../../global/vendor/asscrollable/asScrollable.css",
        "../../global/vendor/switchery/switchery.css",
        "../../global/vendor/intro-js/introjs.css",
        "../../global/vendor/slidepanel/slidePanel.css",
        "../../global/vendor/flag-icon-css/flag-icon.css",
        "../../global/vendor/chartist/chartist.css",
        "../../global/vendor/jvectormap/jquery-jvectormap.css",
        "../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css",
        "../assets/examples/css/dashboard/v1.css",



        "../../global/fonts/weather-icons/weather-icons.css",
        "../../global/fonts/web-icons/web-icons.min.css",
        "../../global/fonts/brand-icons/brand-icons.min.css",
        'http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic',


    ];
    public $js = [
        /*"../../global/vendor/html5shiv/html5shiv.min.js",
        "../../global/vendor/media-match/media.match.min.js",
        '../../global/vendor/respond/respond.min.js',
        '../../global/vendor/breakpoints/breakpoints.js'*/
        "../../global/vendor/babel-external-helpers/babel-external-helpers.js",
        "../../global/vendor/jquery/jquery.js",
        "../../global/vendor/popper-js/umd/popper.min.js",
        "../../global/vendor/bootstrap/bootstrap.js",
        "../../global/vendor/animsition/animsition.js",
        "../../global/vendor/mousewheel/jquery.mousewheel.js",
        "../../global/vendor/asscrollbar/jquery-asScrollbar.js",
        "../../global/vendor/asscrollable/jquery-asScrollable.js",
        "../../global/vendor/ashoverscroll/jquery-asHoverScroll.js",

        "../../global/vendor/switchery/switchery.js",
        "../../global/vendor/intro-js/intro.js",
        "../../global/vendor/screenfull/screenfull.js",
        "../../global/vendor/slidepanel/jquery-slidePanel.js",
        "../../global/vendor/skycons/skycons.js",
        "../../global/vendor/chartist/chartist.min.js",
        "../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js",
        "../../global/vendor/aspieprogress/jquery-asPieProgress.min.js",
        "../../global/vendor/jvectormap/jquery-jvectormap.min.js",
        "../../global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js",
        "../../global/vendor/matchheight/jquery.matchHeight-min.js",

        "../../global/js/Component.js",
        "../../global/js/Plugin.js",
        "../../global/js/Base.js",
        "../../global/js/Config.js",

        "../assets/js/Section/Menubar.js",
        "../assets/js/Section/GridMenu.js",
        "../assets/js/Section/Sidebar.js",
        "../assets/js/Section/PageAside.js",
        "../assets/js/Plugin/menu.js",

        "../../global/js/config/colors.js",
        "../assets/js/config/tour.js",

        "../assets/js/Site.js",
        "../../global/js/Plugin/asscrollable.js",
        "../../global/js/Plugin/slidepanel.js",
        "../../global/js/Plugin/switchery.js",
        "../../global/js/Plugin/matchheight.js",
        "../../global/js/Plugin/jvectormap.js",

        "../assets/examples/js/dashboard/v1.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
