<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkTimeDoctor */

$this->title = 'Create Work Time Doctor';
$this->params['breadcrumbs'][] = ['label' => 'Work Time Doctors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-time-doctor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
