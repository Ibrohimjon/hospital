<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkTimeDoctor */

$this->title = 'Update Work Time Doctor: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Time Doctors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-time-doctor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
