<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkTimeDevice */

$this->title = 'Create Work Time Device';
$this->params['breadcrumbs'][] = ['label' => 'Work Time Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-time-device-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
