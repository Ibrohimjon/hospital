<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WorkTimeDevice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-time-device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_id')->textInput() ?>

    <?= $form->field($model, 'mon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tues')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'frid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
